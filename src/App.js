import React from 'react';
import { withRouter, Switch, Route } from 'react-router-dom';
import "react-loader-spinner/dist/loader/css/react-spinner-loader.css"

// Styles
import './App.css';

// Components
import Toolbar from './components/Toolbar';
import Landing from './components/Landing';
import Menu from './components/Menu';

function MenuContainer({history, match}) {
  const [title, setTitle] = React.useState(false);

  return (
    <>
      <Toolbar title={title} />
      <Menu match={match} history={history} onTitleChange={(v) => setTitle(v)} />
    </>
  )
}

class App extends React.Component {
  state = {
    history: false,
    title: null
  };

  constructor(props) {
    super(props);

    this.state = {
      history: props.history
    }
  }

  setTitle(title) {
    this.setState({
      title
    })
  }

  render() {
    return (
      <React.Fragment>
        <Switch>
          <Route exact path="/">
            <Toolbar title={false} />
            <Landing />
          </Route>
          <Route path="/:id" component={MenuContainer} />
        </Switch>
      </React.Fragment>
    )
  }
}

export default withRouter(App);
