import React from 'react';

// Style
import '../styles/Toolbar.css';

function Toolbar({history, search, title}) {

  return (
    <nav className="flexible defaultRowContainer">
      <div className="spacer4"></div>
      <h2 className={"white" + (!title ? ' dancing' : '')}>{title ? title : 'QuickSafe'}</h2>
    </nav>
  );
}

export default Toolbar;
