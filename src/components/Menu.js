import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faFacebookSquare, faInstagram } from '@fortawesome/free-brands-svg-icons'
import { faMobileAlt, faCar, faHeart, faBone, faUtensils } from '@fortawesome/free-solid-svg-icons'
import Loader from 'react-loader-spinner'

// Styles
import '../styles/Menu.css';

// Components

function Menu({history, match, onTitleChange}) {
  const [isLoading, setIsLoading] = React.useState(true);
  const [spectate, setSpectate] = React.useState(false);
  const [sections, setSections] = React.useState([]);
  const [socials, setSocials] = React.useState([]);
  const [sectionsOpened, setSectionsOpened] = React.useState(false);

  const onSpectate = (url) => {
    setSpectate(url);
  }

  const load = async (endpoint) => {
    setIsLoading(true);
    setSections([]);
    setSocials([]);

    try {
      let res = await fetch(`https://res.cloudinary.com/lgxy/raw/upload/quicksafe/menus/${endpoint}.json`);
      if (!res.ok) throw 'error';
      res = await res.json();

      onTitleChange(res.name);
      setSocials(res.socials);
      setSections(res.sections);
      setIsLoading(false);
    } catch(e) {
      onTitleChange(false);
      setIsLoading(false);
    }
  }

  const GoTo = async (point) => {
    setSectionsOpened(false);
    let section = document.getElementById(point);
    if (!section) return;

    window.scrollTo(section);
  };

  React.useEffect(() => {
    history.listen((data) => {
      GoTo(data.hash.slice(1));
    });
  }, [])

  React.useEffect(() => {
    load(match.params.id);
  }, [match.params.id]);

  return (
    <div className="flexible defaultSection padded defaultColumnContainer">
      {
        isLoading &&
        <Loader type="BallTriangle" width={40} color="#212121" className="lgxy-loader"/>
      }
      {
        !isLoading && sections.length === 0 &&
        <>
          <div className="spacer"></div>
          <h1 className="red insetWidth centerText">
            <FontAwesomeIcon className="primary medicon" icon={faBone} />
          </h1>
          <div className="spacer2"></div>
          <h3 className="red insetWidth centerText">Menu Not Available</h3>
          <div className="spacer40"></div>
          <div className="spacer"></div>
        </>
      }
      {
        sections.map((section, index) => {
          return (
            <div key={"SECTION"+index} id={section.param}>
              <h2 key={"SECTION_TITLE"+index} className="primary">{section.title}</h2>
              <div key={"SECTION_ITEMS_CONTAINER"+index} className="lgxy-card flexible defaultColumnContainer">
                {
                  section.items.map((item, i) => {
                    if (item.image) {
                      return (
                        <div key={`SECTION_ITEM_${index}_${i}`} className="defaultColumnContainer item clickable fullWidth" onClick={() => onSpectate(item.image)}>
                          <h3 key={`SECTION_ITEM_NAME_${index}_${i}`} className="primary">{item.name} <FontAwesomeIcon className="red" icon={faHeart} /></h3>
                          <p key={`SECTION_ITEM_PRICE_${index}_${i}`} className="insetWidth primary">{item.prices.join(' | ')}</p>
                        </div>
                      )
                    }

                    return (
                      <div key={`SECTION_ITEM_${index}_${i}`} className="defaultColumnContainer item fullWidth">
                        <h3 key={`SECTION_ITEM_NAME_${index}_${i}`} className="primary">{item.name}</h3>
                        <p key={`SECTION_ITEM_PRICE_${index}_${i}`} className="insetWidth primary">{item.prices.join(' | ')}</p>
                      </div>
                    )
                  })
                }
              </div>
            </div>
          )
        })
      }
      <div className={"lgxy-spectate flexible center" + (spectate ? ' active' : '')} onClick={() => onSpectate(false)}>
        {
          spectate &&
          <img src={spectate} alt="spectated image"/>
        }
      </div>
      <div className={"lgxy-spectate flexible center" + (sectionsOpened ? ' active' : '')} onClick={() => setSectionsOpened(false)}>
        {
          sectionsOpened && sections.map((section) => {
            return (
              <h3 onClick={() => GoTo(section.param)} key={"SECTION_TAB"+section.param} className="tertiary insetWidth centerText">{section.title.toUpperCase()}</h3>
            )
          })
        }
      </div>
      {
        sections.length > 0 &&
        <div className="lgxy-links flexible center">
          {
            socials.map((social, index) => {
              if (social.type === 'img') {
                return (
                  <>
                    <img key={"SOCIAL"+index} src={social.url} alt="logo"/>
                    <div key={"SOCIAL_SPACER"+index} className="spacer4"></div>
                  </>
                )
              }

              if (social.type === 'fb') {
                return (
                  <a key={"SOCIAL"+index} href={social.url} rel="noopener noreferrer" target="_blank">
                    <FontAwesomeIcon key={"SOCIAL_ICON"+index} className="icon clickable" icon={faFacebookSquare} />
                  </a>
                )
              }

              if (social.type === 'ig') {
                return (
                  <a key={"SOCIAL"+index} href={social.url} rel="noopener noreferrer" target="_blank">
                    <FontAwesomeIcon key={"SOCIAL_ICON"+index} className="icon clickable" icon={faInstagram} />
                  </a>
                )
              }

              if (social.type === 'tel') {
                return (
                  <a key={"SOCIAL"+index} tel={social.url} rel="noopener noreferrer" target="_blank">
                    <FontAwesomeIcon key={"SOCIAL_ICON"+index} className="icon clickable" icon={faMobileAlt} />
                  </a>
                )
              }

              if (social.type === 'loc') {
                return (
                  <a key={"SOCIAL"+index} href={social.url} rel="noopener noreferrer" target="_blank">
                    <FontAwesomeIcon key={"SOCIAL_ICON"+index} className="icon clickable" icon={faCar} />
                  </a>
                )
              }

              return (
                <></>
              )
            })
          }
          <FontAwesomeIcon onClick={() => setSectionsOpened(!sectionsOpened)} className="icon clickable tertiary" icon={faUtensils} />
        </div>
      }
    </div>
  )
}

export default Menu;
