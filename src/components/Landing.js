import React from 'react';

//Styles
import '../styles/Landing.css';

function Landing() {
  return (
    <div className="flexible defaultSection padded defaultColumnContainer primaryFill center">
      <div className="spacer"></div>
      <img className="logo" src="https://res.cloudinary.com/lgxy/image/upload/v1600247395/quicksafe/logo.png" alt="logo"/>
      <div className="spacer4"></div>
      <h3 className="insetWidth centerText tertiary">A Quick & Safe Menu</h3>
      <div className="spacer"></div>
      <h5 className="insetWidth centerText tertiary"><a mailto="cre@trinumdesign.com">cre@trinumdesign.com</a></h5>
      <div className="spacer2"></div>
      <h5 className="insetWidth centerText tertiary"><a tel="7737660824">+1 (773) 658-0166</a></h5>
      <div className="spacer4"></div>
    </div>
  )
}

export default Landing;
